# Item Renderer #

Item Renderer is a small mod for Minecraft which allows you export rendered items from Minecraft. This is a rather simple mod that will render your currently selected item to a .png file in .minecraft/rendered when you press P. It's useful if you need quick pictures of an item, for example for a wiki article.

**Requires an OpenGL 3.2 compatible graphics card.** If your graphics card or drivers do not support OpenGL 3.2, this mod will refuse to do anything.

## Credits ##

Kobata: Author, Coder

Zuxelus: Coder

### Downloads ###

Version for MC 1.6.2

https://github.com/Kobata/item-render/releases/download/MC1.6.2/ItemRender-MC1.6.jar

Version for MC 1.7.10

https://bitbucket.org/Zuxelus/item-render/downloads/itemrender-1.7.10.jar