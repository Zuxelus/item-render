package itemrender;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
//import cpw.mods.fml.common.registry.TickRegistry;
import cpw.mods.fml.relauncher.Side;
import itemrender.client.KeyBindings;
import itemrender.client.KeyInputHandler;
import itemrender.client.RenderHandler;
import net.minecraftforge.common.MinecraftForge;
/*import itemrender.client.KeybindRenderEntity;
import itemrender.client.KeybindRenderInventoryBlock;
import itemrender.client.KeybindToggleRender;
import itemrender.client.RenderTickHandler;*/
import net.minecraftforge.common.config.Configuration;

import org.apache.logging.log4j.Logger;
import org.lwjgl.opengl.GLContext;

@Mod(modid = "ItemRender", guiFactory = "itemrender.client.GuiFactory")
public class ItemRenderMod {
    @Mod.Instance("ItemRender")
    public static ItemRenderMod instance;
    public static Configuration config;
    public static KeyInputHandler handler;

    public static boolean gl32_enabled = false;

    public static final int DEFAULT_MAIN_TEXTURE_SIZE = 128;
    public static final int DEFAULT_GRID_TEXTURE_SIZE = 32;

    private static int mainTextureSize;
    private static int gridTextureSize;
	private static String filenameSuffix;
	private static String filenameSuffix2;    

    public Logger log;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e) {
        log = e.getModLog();
        gl32_enabled = GLContext.getCapabilities().OpenGL32;

        config = new Configuration(e.getSuggestedConfigurationFile());
        syncConfig();
    }

    public static void syncConfig() 
    {
        mainTextureSize = config.get(Configuration.CATEGORY_GENERAL, "mainTextureSize", DEFAULT_MAIN_TEXTURE_SIZE).getInt();
        filenameSuffix = config.get(Configuration.CATEGORY_GENERAL, "mainFilenameSuffix", "").getString();
        gridTextureSize = config.get(Configuration.CATEGORY_GENERAL, "gridTextureSize", DEFAULT_GRID_TEXTURE_SIZE).getInt();
        filenameSuffix2 = config.get(Configuration.CATEGORY_GENERAL, "gridFilenameSuffix", "_grid").getString();
        if (config.hasChanged())
        {
        	config.save();
        	KeyInputHandler.ChangeSettings(mainTextureSize, filenameSuffix, gridTextureSize, filenameSuffix2);
        }    	
    }
    
    @Mod.EventHandler
    public void init(FMLInitializationEvent e) {
        if(gl32_enabled) {
        	KeyBindings.init();
        	MinecraftForge.EVENT_BUS.register(new RenderHandler());        	
        	FMLCommonHandler.instance().bus().register(new KeyInputHandler(mainTextureSize, filenameSuffix, gridTextureSize, filenameSuffix2));
        } else {
            log.error("OpenGL 3.2 not detected, mod will not work!");
        }
    }
}

