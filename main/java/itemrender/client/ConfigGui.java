package itemrender.client;

import itemrender.ItemRenderMod;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import cpw.mods.fml.client.config.GuiConfig;

public class ConfigGui extends GuiConfig 
{
    public ConfigGui(GuiScreen parent) 
    {
        super(parent,
                new ConfigElement(ItemRenderMod.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
                "ItemRender", false, false, GuiConfig.getAbridgedConfigPath(ItemRenderMod.config.toString()));
    }
}