package itemrender.client;

import org.lwjgl.input.Keyboard;

import cpw.mods.fml.client.registry.ClientRegistry;
import net.minecraft.client.settings.KeyBinding;

public class KeyBindings {

    public static KeyBinding keybindRenderEntity;
    public static KeyBinding keybindRenderInventoryBlock;
    public static KeyBinding keybindToggleRender;
    

    public static void init() {

    	keybindRenderEntity = new KeyBinding("key.renderentity", Keyboard.KEY_L, "key.categories.itemrender");
    	keybindRenderInventoryBlock = new KeyBinding("key.renderinventoryblock", Keyboard.KEY_P, "key.categories.itemrender");
    	keybindToggleRender = new KeyBinding("key.togglerenderpreview", Keyboard.KEY_O, "key.categories.itemrender");

        // Register both KeyBindings to the ClientRegistry
        ClientRegistry.registerKeyBinding(keybindRenderEntity);
        ClientRegistry.registerKeyBinding(keybindRenderInventoryBlock);
        ClientRegistry.registerKeyBinding(keybindToggleRender);
    }

}
