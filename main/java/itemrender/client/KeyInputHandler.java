package itemrender.client;

import itemrender.ItemRenderMod;
import itemrender.client.rendering.FBOHelper;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.AxisAlignedBB;
import net.minecraftforge.client.ForgeHooksClient;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import cpw.mods.fml.relauncher.ReflectionHelper;

public class KeyInputHandler {

	public static FBOHelper fbo;
	public static FBOHelper fbo2;
	public static FBOHelper fbo3;
	public static FBOHelper fbo4;	
	private static String filenameSuffix = "";
	private static String filenameSuffix2 = "";
	private RenderItem itemRenderer = new RenderItem();

	public KeyInputHandler(int textureSize, String filename_suffix, int textureSize2, String filename_suffix2)
	{
		fbo = new FBOHelper(textureSize);
		fbo2 = new FBOHelper(textureSize2);
		fbo3 = new FBOHelper(textureSize);
		fbo4 = new FBOHelper(textureSize2);		
		filenameSuffix = filename_suffix;
		filenameSuffix2 = filename_suffix2;
	}
	
	public static void ChangeSettings(int textureSize, String filename_suffix, int textureSize2, String filename_suffix2)
	{
		fbo = new FBOHelper(textureSize);
		fbo2 = new FBOHelper(textureSize2);
		fbo3 = new FBOHelper(textureSize);
		fbo4 = new FBOHelper(textureSize2);		
		filenameSuffix = filename_suffix;
		filenameSuffix2 = filename_suffix2;		
	}

	@SubscribeEvent
	public void onKeyInput(InputEvent.KeyInputEvent event) 
	{
		if (KeyBindings.keybindRenderEntity.isPressed())
		{
			KeybindRenderEntityPressed(fbo, filenameSuffix);
			KeybindRenderEntityPressed(fbo2, filenameSuffix2);
		}

		if (KeyBindings.keybindRenderInventoryBlock.isPressed())
		{
			KeybindRenderInventoryBlockPressed(fbo3, filenameSuffix);
			KeybindRenderInventoryBlockPressed(fbo4, filenameSuffix2);
		}

		if (KeyBindings.keybindToggleRender.isPressed())
		{
			RenderHandler.renderPreview = !RenderHandler.renderPreview;
		}
	}

	private void KeybindRenderEntityPressed(FBOHelper fbo_, String filenameSuffix_)
	{
		Minecraft minecraft = FMLClientHandler.instance().getClient();
		if (minecraft.pointedEntity != null) {
			EntityLivingBase current = (EntityLivingBase)minecraft.pointedEntity;
			fbo_.begin();

			AxisAlignedBB aabb = current.boundingBox;
			double minX = aabb.minX - current.posX;
			double maxX = aabb.maxX - current.posX;
			double minY = aabb.minY - current.posY;
			double maxY = aabb.maxY - current.posY;
			double minZ = aabb.minZ - current.posZ;
			double maxZ = aabb.maxZ - current.posZ;

			double minBound = Math.min(minX, Math.min(minY, minZ));
			double maxBound = Math.max(maxX, Math.max(maxY, maxZ));

			double boundLimit = Math.max(Math.abs(minBound), Math.abs(maxBound));

			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glPushMatrix();
			GL11.glLoadIdentity();
			GL11.glOrtho(-boundLimit*0.75, boundLimit*0.75, -boundLimit*1.25, boundLimit*0.25, -100.0, 100.0);

			GL11.glMatrixMode(GL11.GL_MODELVIEW);

			renderEntity(current);
			//GuiInventory.func_110423_a(0, 0, 1, 1, 1, current);

			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glPopMatrix();

			fbo_.end();

			fbo_.saveToFile(new File(minecraft.mcDataDir,
					String.format("rendered/entity_%s%s.png", EntityList.getEntityString(current), filenameSuffix_)));

			fbo_.restoreTexture();
		}
	}

	private void KeybindRenderInventoryBlockPressed(FBOHelper fbo_, String filenameSuffix_)
	{
		Minecraft minecraft = FMLClientHandler.instance().getClient();
		if (minecraft.thePlayer != null) {
			ItemStack current = minecraft.thePlayer.getCurrentEquippedItem();
			if(current != null && current.getItem() != null) {

				fbo_.begin();

				GL11.glMatrixMode(GL11.GL_PROJECTION);
				GL11.glPushMatrix();
				GL11.glLoadIdentity();
				GL11.glOrtho(0, 16, 0, 16, -100.0, 100.0);

				GL11.glMatrixMode(GL11.GL_MODELVIEW);

				RenderHelper.enableGUIStandardItemLighting();

				RenderBlocks renderBlocks = ReflectionHelper.getPrivateValue(Render.class, itemRenderer,"field_147909_c","renderBlocks");
				if(!ForgeHooksClient.renderInventoryItem(renderBlocks, minecraft.renderEngine, current, true, 0.0f,
								(float) 0, (float) 0)) 
				{
					itemRenderer.renderItemIntoGUI(null, minecraft.renderEngine, current, 0, 0);
				}

				GL11.glMatrixMode(GL11.GL_PROJECTION);
				GL11.glPopMatrix();

				RenderHelper.disableStandardItemLighting();

				fbo_.end();

				fbo_.saveToFile(new File(minecraft.mcDataDir,
						String.format("rendered/item_%d_%d%s.png", Item.getIdFromItem(current.getItem()), current.getItemDamage(), filenameSuffix_)));

				fbo_.restoreTexture();
			}
		}
	}

	private void renderEntity(EntityLivingBase entity) {

		GL11.glEnable(GL11.GL_COLOR_MATERIAL);
		GL11.glPushMatrix();
		GL11.glScalef((float)(-1), (float)1, (float)1);
		GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
		float f2 = entity.renderYawOffset;
		float f3 = entity.rotationYaw;
		float f4 = entity.rotationPitch;
		float f5 = entity.prevRotationYawHead;
		float f6 = entity.rotationYawHead;
		GL11.glRotatef(135.0F, 0.0F, 1.0F, 0.0F);
		RenderHelper.enableStandardItemLighting();
		GL11.glRotatef(-135.0F, 0.0F, 1.0F, 0.0F);

		GL11.glRotatef((float)Math.toDegrees(Math.asin(Math.tan(Math.toRadians(30)))), 1.0F, 0.0F, 0.0F);
		GL11.glRotatef(-45, 0.0F, 1.0F, 0.0F);

		entity.renderYawOffset = (float)Math.atan((double)(1 / 40.0F)) * 20.0F;
		entity.rotationYaw = (float)Math.atan((double)(1 / 40.0F)) * 40.0F;
		entity.rotationPitch = -((float)Math.atan((double)(1 / 40.0F))) * 20.0F;
		entity.rotationYawHead = entity.rotationYaw;
		entity.prevRotationYawHead = entity.rotationYaw;
		GL11.glTranslatef(0.0F, entity.yOffset, 0.0F);
		RenderManager.instance.playerViewY = 180.0F;
		RenderManager.instance.renderEntityWithPosYaw(entity, 0.0D, 0.0D, 0.0D, 0.0F, 1.0F);
		entity.renderYawOffset = f2;
		entity.rotationYaw = f3;
		entity.rotationPitch = f4;
		entity.prevRotationYawHead = f5;
		entity.rotationYawHead = f6;
		GL11.glPopMatrix();
		RenderHelper.disableStandardItemLighting();
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
	}
	
    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
    {
         if (event.modID.equals("ItemRender"))
         {
         	ItemRenderMod.syncConfig();
         }
    }  
}
