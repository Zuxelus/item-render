package itemrender.client;

import itemrender.ItemRenderMod;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

import org.lwjgl.opengl.GL11;

import cpw.mods.fml.client.event.ConfigChangedEvent;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;

public class RenderHandler
{
	public static boolean renderPreview = false;
	
	@SubscribeEvent
	public void Draw(RenderGameOverlayEvent event) 
	{
		if (renderPreview)
		{
			int originalTexture = GL11.glGetInteger(GL11.GL_TEXTURE_BINDING_2D);

			// Bind framebuffer texture
			KeyInputHandler.fbo3.bind();
			GL11.glBegin(GL11.GL_QUADS);
			GL11.glTexCoord2f(0, 0);
			GL11.glVertex2i(0, 0);
			GL11.glTexCoord2f(0, 1);
			GL11.glVertex2i(0, 128);
			GL11.glTexCoord2f(1, 1);
			GL11.glVertex2i(128, 128);
			GL11.glTexCoord2f(1, 0);
			GL11.glVertex2i(128, 0);
			GL11.glEnd();

			// Restore old texture
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, originalTexture);
		}
	}
/*	
	   @SubscribeEvent
	   public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) 
	   {
	        if (event.modID.equals("ItemRender"))
	        {
	        	ItemRenderMod.syncConfig();
	        }
	   }*/   
}
